package fr.epita.android.velibproject.Tools;

import java.util.ArrayList;

import fr.epita.android.velibproject.Model.Station;

/**
 * Created by Straky on 15/05/2017.
 */
public class SearchFilter {

    public static ArrayList<Station> getStationFromStreet(String searchText, ArrayList<Station> stations) {
        ArrayList<Station> newStations = new ArrayList<>();
        for (Station station : stations) {
            if (station.getField().getAddress().toLowerCase().contains(searchText.toLowerCase())) {
                newStations.add(station);
            }
        }
        return newStations;
    }

}
