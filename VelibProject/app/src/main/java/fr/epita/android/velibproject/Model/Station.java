package fr.epita.android.velibproject.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Straky on 12/05/2017.
 */
public class Station implements Serializable {

    @SerializedName("recordid")
    private String id;

    @SerializedName("fields")
    private Fields field;

    public Station(Fields fields) {
        this.field = fields;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Fields getField() {
        return field;
    }

    public void setField(Fields field) {
        this.field = field;
    }
}
