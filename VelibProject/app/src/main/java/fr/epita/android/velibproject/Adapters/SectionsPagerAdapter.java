package fr.epita.android.velibproject.Adapters;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import fr.epita.android.velibproject.Model.Station;
import fr.epita.android.velibproject.R;


/**
 * Created by Will on 14/05/2017.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {


    protected static ArrayList<Station> stations;

    public SectionsPagerAdapter(FragmentManager fm, ArrayList<Station> stationList) {
        super(fm);

        Log.d("resp", "INIT");

        stations = stationList;
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        private View rootView;
        private Station station;
        private Integer posi;

        public static PlaceholderFragment newInstance(int sectionNumber, ArrayList<Station> stations) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            rootView = inflater.inflate(R.layout.station_fragment, container, false);

            posi = (Integer) getArguments().get(ARG_SECTION_NUMBER);
            if (posi == null)
                //bon ba c'est casse, on va commencer au debut hein
                posi = 0;
            else {
                //pck sinon on incremente de un en trop dans le tableau (ba ouais un tableau ca commence a 0 et l'indexation ici commence a 1)
                posi -= 1;
            }
            station = stations.get(posi);



            InitView();

            return rootView;
        }

        private void InitView() {
            TextView textView = (TextView) rootView.findViewById(R.id.sectorText);
            final String name = "Station " + station.getField().getName() + " at address " + station.getField().getAddress();
            //textView.setText(name);

            Log.d("w",posi.toString());
            TextView tvname = (TextView)rootView.findViewById(R.id.frag_station_name);
            String nameText = "Station " + station.getField().getName();
            tvname.setText(nameText);

            TextView tvaddress = (TextView)rootView.findViewById(R.id.frag_station_address);
            String addressText = "Adresse : " + station.getField().getAddress();
            tvaddress.setText(addressText);

            TextView tvbikestands = (TextView)rootView.findViewById(R.id.frag_station_bike_stands);
            String bikeText = "Places maximum : " + station.getField().getBike_stands();
            tvbikestands.setText(bikeText);

            TextView tvavailablestands = (TextView)rootView.findViewById(R.id.frag_station_available_bike_stands);
            String bikeAvailable = "Places disponibles : " + station.getField().getAvailable_bike_stands();
            tvavailablestands.setText(bikeAvailable);

            ImageView imgstatus = (ImageView)rootView.findViewById(R.id.frag_station_status);
            TextView textStatus = (TextView)rootView.findViewById(R.id.frag_station_status_text);
            if (station.getField().getStatus().equals("OPEN")) {
                imgstatus.setImageResource(R.mipmap.ic_greendot);
                textStatus.setText(getResources().getText(R.string.open));
            }
            else {
                imgstatus.setImageResource(R.mipmap.ic_reddot);
                textStatus.setText(getResources().getText(R.string.closed));
            }

            ImageButton btn = (ImageButton)rootView.findViewById(R.id.viewPageBtnGotoMaps);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String address = null;
                    try {
                        address = URLEncoder.encode(station.getField().getAddress(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String url = "geo:0,0?q=" + address;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    getContext().startActivity(intent);
                }
            });
        }
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("POS", String.valueOf(position));
        return PlaceholderFragment.newInstance(position + 1, null);
    }

    @Override
    public int getCount() {
        return stations.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SECTION 1";
            case 1:
                return "SECTION 2";
            case 2:
                return "SECTION 3";
        }
        return null;
    }


}