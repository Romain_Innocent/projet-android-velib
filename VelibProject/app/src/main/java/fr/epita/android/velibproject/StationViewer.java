package fr.epita.android.velibproject;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import fr.epita.android.velibproject.Adapters.SectionsPagerAdapter;
import fr.epita.android.velibproject.Model.Station;

/**
 * Created by Will on 14/05/2017.
 */


public class StationViewer extends AppCompatActivity {

    ViewPager viewPager;
    ArrayList<Station> stations;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.station_pageviewer);

        int position = getIntent().getIntExtra("position", 0);
        stations = (ArrayList<Station>) getIntent().getSerializableExtra("stations");

        //Si un filtre a ete applique, on utilise cette liste, sinon on utilise la liste de toutes les stations
//        if (stations_filter != null)
//            stations = stations_filter;

        SectionsPagerAdapter mSectionsPagerAdapter = new
                SectionsPagerAdapter(getSupportFragmentManager(), stations);
        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.setCurrentItem(position);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar_page_viewer);
        setSupportActionBar(myToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.station_view_activity_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                Toast.makeText(this, "Current page : " + stations.get(viewPager.getCurrentItem()).getField().getName(), Toast.LENGTH_SHORT).show();

                String title = "Title to share";//Title you wants to share
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, title);
                shareIntent.putExtra(Intent.EXTRA_TEXT, getStationInfos());
                shareIntent.setType("*/*");
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(Intent.createChooser(shareIntent,"Select app to share station"));

                return true;
            case R.id.action_groupe:
                Intent intent = new Intent(this, GroupView.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private String getStationInfos() {
        Station station = stations.get(viewPager.getCurrentItem());

        String newline = System.lineSeparator();
        StringBuilder sb = new StringBuilder("Station : ");

        //Nom de la station
        sb.append(station.getField().getName());
        sb.append(newline);

        //Status de la station
        sb.append("Status : ");
        sb.append(station.getField().getStatus());
        sb.append(newline);

        //Nombre de places dans la station
        sb.append("Nombre de places max : ");
        sb.append(station.getField().getBike_stands());
        sb.append(newline);

        //Nombre de places disponibles de la station
        sb.append("Nombre de places disponibles : ");
        sb.append(station.getField().getAvailable_bike_stands());
        sb.append(newline);

        //Adresse de la station
        sb.append("Adresse : ");
        sb.append(station.getField().getAddress());
        sb.append(newline);

        //Date de mise a jour de la station
        sb.append("Derniere mise a jour : ");
        sb.append(station.getField().getLast_updated());
        sb.append(newline);

        return sb.toString();
    }
}
