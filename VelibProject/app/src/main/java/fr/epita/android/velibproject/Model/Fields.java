package fr.epita.android.velibproject.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Will on 15/05/2017.
 */

public class Fields implements Serializable {

    @SerializedName("status")
    private String status = "";

    @SerializedName("contract_name")
    private String city = "";

    @SerializedName("name")
    private String name = "";

    @SerializedName("bike_stands")
    private String bike_stands = "";

    @SerializedName("last_update")
    private String last_updated = "";

    @SerializedName("available_bike_stands")
    private String available_bike_stands = "";

    @SerializedName("available_bikes")
    private String available_bikes = "";

    @SerializedName("address")
    private String address = "";

    public Fields(String status, String city, String name, String bike_stands, String last_updated,
                  String available_bike_stands, String available_bikes, String address) {
        this.status = status;
        this.city = city;
        this.name = name;
        this.bike_stands = bike_stands;
        this.last_updated = last_updated;
        this.available_bike_stands = available_bike_stands;
        this.available_bikes = available_bikes;
        this.address = address;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBike_stands(String bike_stands) {
        this.bike_stands = bike_stands;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public void setAvailable_bike_stands(String available_bike_stands) {
        this.available_bike_stands = available_bike_stands;
    }

    public void setAvailable_bikes(String available_bikes) {
        this.available_bikes = available_bikes;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getBike_stands() {
        return bike_stands;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public String getAvailable_bike_stands() {
        return available_bike_stands;
    }

    public String getAvailable_bikes() {
        return available_bikes;
    }

    public String getAddress() {
        return address;
    }


}
