package fr.epita.android.velibproject.RetrofitWebService;

/**
 * Created by Will on 15/05/2017.
 */

import fr.epita.android.velibproject.Model.Records;
import retrofit2.Call;
import retrofit2.http.GET;

public interface StationServiceInterface {
    public final String URL = "https://opendata.paris.fr/";


    @GET("api/records/1.0/search/?dataset=stations-velib-disponibilites-en-temps-reel&rows=500")
    Call<Records> getStations();
}
