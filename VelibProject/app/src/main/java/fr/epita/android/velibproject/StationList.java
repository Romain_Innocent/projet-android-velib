package fr.epita.android.velibproject;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import fr.epita.android.velibproject.Adapters.StationAdapter;
import fr.epita.android.velibproject.Model.Records;
import fr.epita.android.velibproject.Model.Station;
import fr.epita.android.velibproject.Tools.RetrofitCallApi;
import fr.epita.android.velibproject.Tools.SearchFilter;

public class StationList extends AppCompatActivity {

    private RecyclerView.Adapter adapter;
    private ArrayList<Station> stations;
    private ArrayList<Station> stations_filter;
    RetrofitCallApi retrofitCallApi;
    private SearchView searchView;
    private TextView errorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_list);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar_station_list);
        setSupportActionBar(myToolbar);

        InitView();
        retrofitCallApi = new RetrofitCallApi(this);
        loadStations();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                UpdateList(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                UpdateList(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                loadStations();
                return true;
            case R.id.action_groupe:
                Intent intent = new Intent(this, GroupView.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private RecyclerView recyclerView;

    private void InitView() {
        recyclerView = (RecyclerView)findViewById(R.id.recycleView);
        RecyclerView.LayoutManager layoutManager;
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        errorText = (TextView)findViewById(R.id.textError);
        errorText.setVisibility(View.INVISIBLE);
    }

    private void UpdateList(String searchText) {
        //Pour ne pas crasher quand la liste se charge
        if (stations == null)
            return;

        stations_filter = SearchFilter.getStationFromStreet(searchText, stations);
        adapter = new StationAdapter(this, stations_filter);
        recyclerView.setAdapter(adapter);

    }

    private void loadStations() {
        if (isNetworkAvailable()) {
            findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
            findViewById(R.id.loadingView).setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            errorText.setVisibility(View.INVISIBLE);
            retrofitCallApi.setStations();
            if (searchView != null) {
                searchView.setQuery("", false);
                searchView.clearFocus();
            }
        }
        else {
            setErrorMessage("Error : Check your internet connection.");
        }
    }

    public void setRecyclerView(Records records) {
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        findViewById(R.id.loadingView).setVisibility(View.GONE);
        stations = records.getStations();
        adapter = new StationAdapter(this, stations);
        recyclerView.setAdapter(adapter);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void setErrorMessage(String errorMessage) {
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        findViewById(R.id.loadingView).setVisibility(View.GONE);
        recyclerView.setVisibility(View.INVISIBLE);
        errorText.setVisibility(View.VISIBLE);
        errorText.setText(errorMessage);
    }
}
