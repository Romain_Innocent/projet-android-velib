package fr.epita.android.velibproject.Tools;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.epita.android.velibproject.Model.Records;
import fr.epita.android.velibproject.RetrofitWebService.StationServiceInterface;
import fr.epita.android.velibproject.StationList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Straky on 15/05/2017.
 */
public class RetrofitCallApi {

    private StationList stationList;

    public RetrofitCallApi(StationList stationList) {
        this.stationList = stationList;
    }

    public void setStations() {
        final Records[] records = {null};

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-dd-MM'T'HH:mm")
                .create();

        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl(StationServiceInterface.URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

        StationServiceInterface stService = retrofit.create(StationServiceInterface.class);

        Call<Records> call = stService.getStations();
        
        call.enqueue(new Callback<Records>() {
            @Override
            public void onResponse(Call<Records> call, Response<Records> response) {
                if (response.isSuccessful()) {
                    Log.d("OnResp", String.valueOf(response.code()));
                    records[0] = response.body();
                    stationList.setRecyclerView(response.body());
                }
                else {
                    Log.d("FAIL", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<Records> call, Throwable t) {
                Log.d("FAIL", "OnFailure" + t.getMessage());
                stationList.setErrorMessage("Error : Could not retrieve data from server.");
            }
        });
    }
}
