package fr.epita.android.velibproject.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Will on 15/05/2017.
 */

public class Records {

    @SerializedName("records")
    private ArrayList<Station> stationList;

    public Records(ArrayList<Station> stations) {
        this.stationList = stations;
    }

    public ArrayList<Station> getStations() {
        return stationList;
    }

    public void setStations(ArrayList<Station> stations) {
        this.stationList = stations;
    }
}
