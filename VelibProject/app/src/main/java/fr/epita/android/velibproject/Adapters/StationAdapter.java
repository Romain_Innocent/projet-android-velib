package fr.epita.android.velibproject.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.epita.android.velibproject.Model.Station;
import fr.epita.android.velibproject.R;
import fr.epita.android.velibproject.StationViewer;

/**
 * Created by Straky on 12/05/2017.
 */
public class StationAdapter extends RecyclerView.Adapter<StationAdapter.MyViewHolder> {

    private ArrayList<Station> stations;
    private LayoutInflater inflater;
    private Context ctx;

    public StationAdapter(Context ctx, ArrayList<Station> stations) {
        inflater = LayoutInflater.from(ctx);
        this.stations = stations;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.station_view, parent, false);
        MyViewHolder holder = new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Station station = stations.get(position);
        holder.setProperties(station, position);
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvname;
        TextView tvdescription;
        ImageView img;
        Station station;
        int position;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, StationViewer.class);
                    intent.putExtra("position", position);
                    intent.putExtra("stations", stations);
                    ctx.startActivity(intent);
                }
            });
            tvname = (TextView)itemView.findViewById(R.id.name_station);
            tvdescription = (TextView)itemView.findViewById(R.id.description_station);
            img = (ImageView)itemView.findViewById(R.id.station_view_status_img);
        }
        public void setProperties(Station station, int position) {
            this.station = station;
            this.tvname.setText(station.getField().getName());
            this.tvdescription.setText(station.getField().getStatus());
            if (station.getField().getStatus().equals("OPEN")) {
                img.setImageResource(R.mipmap.ic_greendot);
            } else {
                img.setImageResource(R.mipmap.ic_reddot);
            }
            this.position = position;


        }
    }
}
